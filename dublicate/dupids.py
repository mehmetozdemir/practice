#!/usr/bin/env python3


def find_dublicated_users(filename):
    """Find duplicated users ids from the passwd file."""
    users = {}
    with open(filename, "r", encoding="utf-8") as passwd_file:
        for line in passwd_file:
            # Check the line
            if not line:
                continue
            if line.startswith("#"):
                continue
            # Parse the file
            user_name, user_id = line.split(":")[:2]
            if user_id in users:
                users[user_id].append(user_name)
            else:
                users[user_id] = [user_name]

    # User list is ready, find duplicates
    for user_id, user_names in users.items():
        if len(user_names) < 2:
            continue
        names = ", ".join(user_names)
        print(f"{user_id}: {names}")


if __name__ == "__main__":
    find_dublicated_users("passwd.txt")
