--Practive

# Dublicate

Run command : ``` ./dublicate.py ```

Linux one line command : ``` awk -F: 'n=x[$2]{print n"\n"$0;} {x[$2]=$0;}' passwd.txt | awk -F: '{ print $2 }' ```

# App

Install requirements.txt : ``` pip install -r requirements.txt ```

Run app:                   ``` python app.py ```

http://0.0.0.0:8080 --> HTTP 200 Return

http://0.0.0.0:8080/status -> HTTP 204 Return

Run Docker Build : ``` docker build -t practice ./app ```

Run Docker :       ``` docker run -p 8080:8080 -d practice ```


Kubernetes Running : ``` kubectl apply -f . ```


###
Install google clouds sdk 

https://cloud.google.com/sdk/docs/install follow documantation

# gcloud mysql commit

``` gcloud sql instances create mysql1 \ --tier=db-n1-standard-2 --region=us-central1 ```

``` gcloud sql users set-password root \ --host=% --instance INSTANCE_NAME --password PASSWORD ```

Externall ip : 35.223.2.244

# Google cloud registert auth and image push google container registery 
 
``` gcloud auth login ```

``` docker tag dockerimage gcr.io/my-project/dockerimage ```

``` docker push gcr.io/my-project/dockerimage ```

# Gitlab-ci connection google container registery

gcloud ui -> service account create key

encrypt key command : ``` base64 -b0 key.json  > ~/key.json ```


Finally

gitlab ui -> settings -> CI/CD -> Variables (Add key.json)

# Run gitlab-ci.yaml

master branch pushed commit run gitlab-ci pipeline