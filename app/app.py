from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello():
    return 'Welcome to my app'

@app.route('/status')
def login():
    return 'Status Page', 204

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int("8080"), debug=True)